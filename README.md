# Introduction

This repository serves as a pool for RoboFish group artifacts ([packages](https://git.imp.fu-berlin.de/bioroboticslab/robofish/artifacts/-/packages) and [container images](https://git.imp.fu-berlin.de/bioroboticslab/robofish/artifacts/container_registry)).

## Python packages

In order to install python packages, use `https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple` as an index url. For example, add the following to your [`pip.conf`](https://pip.pypa.io/en/stable/user_guide/#config-file):

```
[global]
extra-index-url = https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple
```
